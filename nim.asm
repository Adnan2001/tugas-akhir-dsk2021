.model SMALL
.code
        ORG 100h
tdata: jmp proses
        lnama db 13,10,'Nama : $'
        lnim db 13,10,'Nim : $

        vnama db 23,?,23 dup(?)
        vnim db 23,?,23 dup(?)

proses:
        mov ah,09h
        lea dx, lnama
        int 21h

        mov ah,0ah
        lea dx, vnama
        int 21h

        mov ah,09h
        lea dx, lnim
        int 21h

        mov ah,0ah
        lea dx, vnim
        int 21h

        lea si,vnama
        lea di,vnim

       
exit : 
        int 20h
end tdata